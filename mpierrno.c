/* 
 * iota - a minimal library for POSIX I/O tracing
 * Copyright 2011-2013, Brown University, Providence, RI. All Rights Reserved.
 * 
 * This file is part of iota.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */


#include <stdio.h>
#include <string.h>
#include <errno.h>

static int mpierrno = 0;
static int mpierrno_rank = -1;
static char mpierrno_msg[80];

void MPI_set_errno(int rank, const char* msg)
{
	mpierrno = errno;
	mpierrno_rank = rank;
	strncpy(mpierrno_msg, msg, 80);
}

__attribute__((destructor))
void MPI_exit_perror()
{
	if (mpierrno) {
	 	mpierrno_msg[79] = '\0';	
		fprintf(stderr, "[rank %d] %s: %s \n",
			mpierrno_rank, mpierrno_msg, strerror(mpierrno));
	}
}

