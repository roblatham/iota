#ifndef __IOTA_WRAP_H__
#define __IOTA_WRAP_H__

#define WRAP_FUNC0(RET, NAME) \
extern RET __real_##NAME();\
RET __wrap_##NAME()

#define WRAP_FUNC1(RET, NAME, ARG1) \
extern RET __real_##NAME(ARG1);\
RET __wrap_##NAME(ARG1)

#define WRAP_FUNC2(RET, NAME, ARG1, ARG2) \
extern RET __real_##NAME(ARG1, ARG2);\
RET __wrap_##NAME(ARG1, ARG2)

#define WRAP_FUNC3(RET, NAME, ARG1, ARG2, ARG3) \
extern RET __real_##NAME(ARG1, ARG2, ARG3);\
RET __wrap_##NAME(ARG1, ARG2, ARG3)

#define WRAP_FUNC4(RET, NAME, ARG1, ARG2, ARG3, ARG4) \
extern RET __real_##NAME(ARG1, ARG2, ARG3, ARG4);\
RET __wrap_##NAME(ARG1, ARG2, ARG3, ARG4)

#define DLSYM(NAME)

#endif

