/* 
 * iota - a minimal library for POSIX I/O tracing
 * Copyright 2011-2013, Brown University, Providence, RI. All Rights Reserved.
 * 
 * This file is part of iota.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */


#ifndef __IOTA_DLSYM_H__
#define __IOTA_DLSYM_H__

#define __USE_GNU
#include <dlfcn.h>

#define WRAP_FUNC0(RET, NAME) \
static RET (*__real_##NAME)() = NULL;\
RET NAME()

#define WRAP_FUNC1(RET, NAME, ARG1) \
static RET (*__real_##NAME)(ARG1) = NULL;\
RET NAME(ARG1)

#define WRAP_FUNC2(RET, NAME, ARG1, ARG2) \
static RET (*__real_##NAME)(ARG1, ARG2) = NULL;\
RET NAME(ARG1, ARG2)

#define WRAP_FUNC3(RET, NAME, ARG1, ARG2, ARG3) \
static RET (*__real_##NAME)(ARG1, ARG2, ARG3) = NULL;\
RET NAME(ARG1, ARG2, ARG3)

#define WRAP_FUNC4(RET, NAME, ARG1, ARG2, ARG3, ARG4) \
static RET (*__real_##NAME)(ARG1, ARG2, ARG3, ARG4) = NULL;\
RET NAME(ARG1, ARG2, ARG3, ARG4)

#define DLSYM(NAME) \
	int recording_save = recording;\
	recording = 0;\
	__real_##NAME = dlsym(RTLD_NEXT, #NAME);\
	char* error;\
	if ((error = dlerror()) != NULL) {\
		fprintf(stderr,\
			"[libiota] error loading symbol '" #NAME "': %s\n", error);\
		exit(1);\
	}\
	recording = recording_save;\
	long reset_usec = get_current_usec();\
	iota_usec += reset_usec - start_usec;\
	start_usec = reset_usec;

#endif

