#!/bin/bash

echo -n "-Wl"
funcs=`nm $1 | grep -Po "real_\w+"`
for f in $funcs
do
	echo -n ",-wrap,${f:5}"
done
echo ""

